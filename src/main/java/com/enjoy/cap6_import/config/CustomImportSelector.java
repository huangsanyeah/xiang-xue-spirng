package com.enjoy.cap6_import.config;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * 自定义ImportSelector
 */
public class CustomImportSelector implements ImportSelector {
    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        //返回【全类名】的bean，注意不能返回Null 否则NPE 可以返回空数组（但是没意义，这个定义的目的就不是为了返回空数组）
        return new String[]{"com.enjoy.cap6_import.bean.Fish", "com.enjoy.cap6_import.bean.Tiger"};
    }
}
