package com.enjoy.cap6_import.config;

import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

import com.enjoy.cap6_import.bean.Pig;

/**
 * 自定义ImportBeanDefinitionRegistrar
 */
public class CustomImportBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {

    /**
     * @param importingClassMetadata 当前类的注解信息
     * @param registry               BeanDefinition注册类,把所有需要添加到容器中的bean加入;
     * @description 如果Dog和Cat同时存在于我们IOC容器中, 那么创建Pig类, 加入到容器
     */
    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        boolean hasDogBean = registry.containsBeanDefinition("com.enjoy.cap6_import.bean.Dog");
        boolean hasCatBean = registry.containsBeanDefinition("com.enjoy.cap6_import.bean.Cat");
        if (hasDogBean && hasCatBean) {
            //注意，需要使用RootBeanDefinition对于我们要注册的bean, 给bean进行封装;参见源码AopConfigUtils，line141
            RootBeanDefinition beanDefinition = new RootBeanDefinition(Pig.class);
            registry.registerBeanDefinition("pig", beanDefinition);
        }
    }

}
