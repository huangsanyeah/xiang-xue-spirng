package com.enjoy.cap6_import.config;

import org.springframework.beans.factory.FactoryBean;

import com.enjoy.cap6_import.bean.Monkey;

/**
 * 自定义类实现FactoryBean接口来创建bean,【工厂模式】
 */
public class CustomFactoryBean implements FactoryBean<Monkey> {

    @Override
    public Monkey getObject() throws Exception {
        // TODO Auto-generated method stub
        return new Monkey();
    }

    @Override
    public Class<?> getObjectType() {
        // TODO Auto-generated method stub
        return Monkey.class;
    }

    /**
     * true是单例,false是多实例
     * @return
     */
    @Override
    public boolean isSingleton() {
        return true;
    }
}
