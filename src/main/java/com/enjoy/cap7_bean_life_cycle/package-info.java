/**
 * bean的生命周期和BeanPostProcessor介绍
 * 1.构造方法创建bean
 * 2.@PostConstruct bean创建完（构造方法 或者new）后进行的初始化操作,属于【JDK】规范的, @Bean(initMethod = "init", destroyMethod = "destroy")是【spring】的
 * 3.InitializingBean接口，afterPropertiesSet方法-当我们的bean属性赋值和初始化完成时调用
 * 4.@PreDestroy: 在bean将被移除之前进行通知, 在容器销毁之前进行清理工作
 * 5.DisposableBean接口，destroy方法，当我们bean销毁时,调用此方法, @Bean(initMethod = "init", destroyMethod = "destroy")
 * 6.BeanPostProcessor后置处理器接口，实现这个接口，类似于AOP,对init方法进行增强,还有ApplicationContextAwareProcessor,
 * BeanValidationPostProcess,InitDestroyAnnotationBeanPostProcessor等
 * 7.Plane类实现ApplicationContextAware接口，用于bean中获取IOC容器即ApplicationContext
 */
package com.enjoy.cap7_bean_life_cycle;
