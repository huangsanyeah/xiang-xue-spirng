package com.enjoy.cap7_bean_life_cycle.config;

import com.enjoy.cap1_xml_to_configuration.Person;
import com.enjoy.cap7_bean_life_cycle.bean.Bike;
import com.enjoy.cap7_bean_life_cycle.bean.Train;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

//注释掉下面这个ComponentScan就不会扫描其他的bean了
@ComponentScan("com.enjoy.cap7_bean_life_cycle.bean")
@Configuration
public class Cap7MainConfigOfLifeCycle {
    @Bean("person")
    public Person person() {
        System.out.println("给容器中添加person.......");
        return new Person("person", 20);
    }

    /**
     * 使用注解指定init destroy方法
     * @Scope("prototype") 多实例的情况下，getBean的时候才会创建 此时并不会创建Bike的Bean
     */
    @Bean(initMethod = "init", destroyMethod = "destory")
    public Bike bike() {
        return new Bike();
    }

    @Bean
    public Train train() {
        return new Train();
    }

}
