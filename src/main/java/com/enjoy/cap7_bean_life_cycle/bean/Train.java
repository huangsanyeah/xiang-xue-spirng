package com.enjoy.cap7_bean_life_cycle.bean;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class Train implements InitializingBean, DisposableBean {
    /**
     * step1:构造方法
     */
    public Train() {
        System.out.println("Train......constructor............");
    }

    /**
     * step2:PostConstruct
     */
    @PostConstruct
    public void init() {
        System.out.println("Train.....@PostConstruct........");
    }

    /**
     * step3:afterPropertiesSet：当我们的bean属性赋值和初始化完成时调用
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("Train.......afterPropertiesSet()...");

    }

	/**
	 * step4:preDestroy
	 */
	@PreDestroy
	public void preDestroy(){
		System.out.println("Train.....@PreDestroy......");
	}

    /**
     * step5:当我们bean销毁时,调用此方法
     */
    @Override
    public void destroy() throws Exception {
        System.out.println("Train......destory......");
        //logger.error
    }

}
