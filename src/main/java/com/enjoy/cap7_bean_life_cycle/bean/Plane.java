package com.enjoy.cap7_bean_life_cycle.bean;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * 实现ApplicationContextAware接口，获取IOC容器ApplicationContext
 * 类似的接口还有BeanFactoryAware,BeanNameAware（cap9: LightAware类）等
 */
@Component
public class Plane implements ApplicationContextAware {
    private ApplicationContext applicationContext;

    public Plane() {
        System.out.println("Plane.....constructor........");
    }

    @PostConstruct
    public void init() {
        System.out.println("Plane.....@PostConstruct........");
    }

    @PreDestroy
    public void destroy() {
        System.out.println("Plane.....@PreDestroy......");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        //将applicationContext传进来,可以拿到
        this.applicationContext = applicationContext;
    }
}
