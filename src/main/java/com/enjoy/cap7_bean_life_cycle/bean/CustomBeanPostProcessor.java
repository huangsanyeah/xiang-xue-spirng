package com.enjoy.cap7_bean_life_cycle.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * BeanPostProcessor,这个是对bean的init方法前后进行增强
 */
@Component
public class CustomBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        //返回一个的对象(传过来的对象)
        //在初始化方法调用之前进行后置处理工作,
        //什么时候调用它:任何初始化方法调用之前(比如在InitializingBean的afterPropertiesSet初始化之前,或自定义init-method调用之前使用
        System.out.println("postProcessBeforeInitialization...." + beanName + "..." + bean);
        return bean;
    }

    //在初始化之后进行后置处理工作, 比如在InitializingBean的afterPropertiesSet()
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("postProcessAfterInitialization...." + beanName + "..." + bean);
        return bean;
    }
}
