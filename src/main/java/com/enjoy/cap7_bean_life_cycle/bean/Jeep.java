package com.enjoy.cap7_bean_life_cycle.bean;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
@Component
public class Jeep {
	public Jeep(){
		System.out.println("Jeep.....constructor........");
	}

	/**
	 * @PostConstruct bean创建完（构造方法 或者new）后进行的初始化操作
	 */
	@PostConstruct
	public void init(){
		System.out.println("Jeep.....@PostConstruct........");
	}
	
	@PreDestroy
	public void destory(){
		System.out.println("Jeep.....@PreDestroy......");
	}
}
