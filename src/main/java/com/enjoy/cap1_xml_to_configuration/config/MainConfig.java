package com.enjoy.cap1_xml_to_configuration.config;

import com.enjoy.cap1_xml_to_configuration.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//配置类====配置文件

/**
 * 使用配置类方式加载Bean到容器中
 */
@Configuration
public class MainConfig {
	/**
	 * 给容器中注册一个bean, 类型为返回值的类型,
	 */
	//@Bean("abcPerson")
	@Bean
	public Person person01(){
		return new Person("james",20);
	}
}
