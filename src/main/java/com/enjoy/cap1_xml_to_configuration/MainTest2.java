package com.enjoy.cap1_xml_to_configuration;

import com.enjoy.cap1_xml_to_configuration.config.MainConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * 扫描@Configuration配置的bean类 注意beanName在不指定的情况下默认是方法名
 */
public class MainTest2 {
    public static void main(String args[]) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(MainConfig.class);
        //从容器中获取bean
//        Person person = (Person) applicationContext.getBean("person01");
//        Person person = (Person) applicationContext.getBean("abcPerson");
//        System.out.println(person);


        String[] namesForBean = applicationContext.getBeanNamesForType(Person.class);
        for (String name : namesForBean) {
        	//此处输出的是MainConfig中默认的@Bean注解方法的【方法名】，此处为person01，如果指定为@Bean("abcPerson") 那么输出abcPerson
            System.out.println(name);
        }

    }
}
