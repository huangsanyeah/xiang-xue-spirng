package com.enjoy.cap3_singleton_prototype.config;

import com.enjoy.cap1_xml_to_configuration.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * 点击进入Scope的scopeName 见如下注释
 * * @see ConfigurableBeanFactory#SCOPE_PROTOTYPE
 * * @see ConfigurableBeanFactory#SCOPE_SINGLETON
 * * @see org.springframework.web.context.WebApplicationContext#SCOPE_REQUEST
 * * @see org.springframework.web.context.WebApplicationContext#SCOPE_SESSION
 */
@Configuration
public class Cap3MainConfig {
    /**
     * prototype：多实例：IOC容器启动的时候，IOC容器并不会去调用方法创建对象，而是每次获取的时候才会调用方法创建对象
     * singleton：单实例【默认】：IOC容器启动的时候会调用方法创建对象并放入容器中，以后每次获取的就是直接从容器中拿（大Map.get）的同一个bean
     * request:主要针对web应用，递交一次请求创建一个实例
     * session:同一个session创建一个实例
     */
    @Scope("prototype")
//    @Scope("singleton") //默认是单例
    @Bean
    public Person person() {
        return new Person("james", 20);
    }
}
