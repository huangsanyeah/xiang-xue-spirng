package com.enjoy.cap10_aop_basic_concepts.aop;

public class Calculator {
	/**
	 * 业务逻辑方法，除法
	 */
	public int div(int i, int j)  {
		System.out.println("Calculator类的目标方法div()开始执行");
		return i/j;
	}
	
}
