/**
 * AOP的基本概念
 * 1.通知方法:
 * * 1.前置通知:logStart(); 在我们执行div()除法之前运行(@Before)
 * * 2.后置通知:logEnd();在我们目标方法div运行结束之后 ,不管有没有异常(@After)
 * * 3.返回通知:logReturn();在我们的目标方法div正常返回值后运行(@AfterReturning)
 * * 4.异常通知:logException();在我们的目标方法div出现异常后运行(@AfterThrowing)
 * * 5.环绕通知:动态代理, 需要手动执行joinPoint.proceed()(其实就是执行我们的目标方法div,), 执行之前div()相当于前置通知, 执行之后就相当于我们后置通知(@Around)
 * 2.test类的输出结果，注意around是在before之前输出的,顺序为：around---before---目标method---around---after---afterReturning
 * 【@Around】:执行目标方法之前...
 * 【@Before】div除法运行....参数列表是:{[4, 2]}
 * Calculator类的目标方法div()开始执行
 * 【@Around】:执行目标方法之后...
 * 【@After】div除法结束......
 * 【@AfterReturning】除法正常返回......运行结果是:{2}
 * div方法的结果是：2
 * 3.AOP源码解读，见第6，7章word笔记，【视频主要看第七章就可以了】
 */
package com.enjoy.cap10_aop_basic_concepts;
