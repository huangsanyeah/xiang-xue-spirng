/**
 * 1.@Conditional注解使用示例
 * 【常见面试题】FactoryBean（注册）和BeanFactory（获取）区别？见2，3
 * 2.BeanFactory是什么？从我们容器中获取实例化后的bean
 * 3.FactoryBean是什么？注册机制，把java实例bean通过FactoryBean注入到容器中
 * 4.editConfigurations中VM options中设置参数为-Dos.name=linux就可以模拟当前环境为linux
 */
package com.enjoy.cap5_conditional;
