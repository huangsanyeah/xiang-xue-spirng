package com.enjoy.cap9_qualifier_primary_resource_inject_annotation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.enjoy.cap9_qualifier_primary_resource_inject_annotation.service.TestService;

@Controller
public class TestController {
	@Autowired
	private TestService testService;
}
