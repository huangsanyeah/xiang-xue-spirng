/**
 * 常用的注解和Aware注入spring底层组件原理（Light类）：
 * 1.@Autowired,@Autowired(required = false)那么容器中不存在这个bean不会报错，@Resource要求必须bean存在。先byName再byType
 * 2.@Qualifier("testDao")，搭配@Autowired指定【名称】来加载
 * 3.@Primary 标记为优先级最高的bean
 * 4.@Resource,1).不支持Primary功能，2).不支持Autowired(required = false) JDK规范
 * 5.@Inject，需要引入第三方javax.inject依赖,不依赖spring
 * JSR-330 'javax.inject.Inject' annotation found and supported for autowiring
 * 和Autowired功能差不多，1).支持@Praimary 2).不支持Autowired(required = false)
 * 6.【面试题：@Qualifier和@Primary区别于联系】
 *
 * @Qualifier就是通过name来查找
 * @Primary多个类要注入同一个bean的时候，那么就使用@Praimary标记这个bean，这样这个bean就不用在每个类重复编写了
 * @Qualifier和@Primary同时存在的时候，用到@Qualifier就通过@Qualifier找，否则就用@Primary标记的bean
 * 7.【面试题：@Autowired,@Resource,@Inject区别与联系】
 * @Autowired spring的，bean没有的话可以设置required = false
 * @Resource JDK的规范（不支持required = false）
 * @Inject 需要引入第三方依赖，非spring得平台可以使用（不支持required = false）
 * 8.【有参】构造也可以注入bean（Son.java）
 * 9.Aware注入spring底层组件原理(LightAware.java)，包含3个部分：
 * 实现 BeanNameAware， ApplicationContextAware，EmbeddedValueResolverAware接口
 * * 1.BeanNameAware获取bean名称
 * * 2.ApplicationContextAware获取spring容器的上下文
 * * 3.EmbeddedValueResolverAware，StringValueResolver:值的解析器
 *
 */
package com.enjoy.cap9_qualifier_primary_resource_inject_annotation;
