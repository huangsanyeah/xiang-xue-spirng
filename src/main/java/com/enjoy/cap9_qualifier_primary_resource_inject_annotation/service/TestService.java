package com.enjoy.cap9_qualifier_primary_resource_inject_annotation.service;

import com.enjoy.cap9_qualifier_primary_resource_inject_annotation.dao.TestDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestService {
    //@Qualifier("testDao")//指定名称来加载

	//@Resource(name = "testDao")
	//@Inject
	@Autowired
	private TestDao testDao;//如果使用Autowired, testDao2, 找到TestDao类型的
	
	public void println(){
		System.out.println("Service...dao...."+testDao);
	}
}
