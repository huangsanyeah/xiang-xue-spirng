package com.enjoy.cap9_qualifier_primary_resource_inject_annotation.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 使用构造器的方式注入bean，即在Sun中注入Moon,这个Moon跟从容器中拿出的Moon是同一个
 * 类：Sun Moon
 */
@Component
public class Sun {
	private Moon moon;

	/**
	 * 在【有参】构造方法中注入Moon的bean
	 * 这个moon是从容器中拿的，注意，对于有参构造器，加不加@Autowired效果都一样
	 * 结论: 不管@Autowired是放到参数, 方法还是构造方法, 都是从容器里取到的bean
	 * @param moon
	 */
	public Sun(@Autowired Moon moon){
		this.moon = moon;
		System.out.println("..Constructor................");
	}
	public Moon getMoon() {
		return moon;
	}

	
	public void setMoon(Moon moon) {
		this.moon = moon;
	}

	@Override
	public String toString() {
		return "Sun [moon=" + moon + "]";
	}
}
