package com.enjoy.cap12_ioc_and_postprocessor.processor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.stereotype.Component;

import com.enjoy.cap9_qualifier_primary_resource_inject_annotation.bean.Moon;

/**
 * BeanDefinitionRegistryPostProcessor
 * 注意观察输出结果：
 * JamesBeanDefinition.postProcessBeanDefinitionRegistry()...bean的数量10
 * JamesBeanDefinitionProcessor..postProcessBeanFactory(),Bean的数量11
 * TODO 首先，最先执行的方法是postProcessBeanDefinitionRegistry，其次，执行postProcessBeanDefinitionRegistry后新增加了一个bean,所以输出结果是10，11
 */
@Component
public class JamesBeanDefinitionRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor {

    /**
     * 这个方法是 BeanDefinitionRegistryPostProcessor父类BeanFactoryPostProcessor的接口方法postProcessBeanFactory()
     * @param beanFactory
     * @throws BeansException
     */
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        //由于postProcessBeanDefinitionRegistry新注册了一个bean,所以这里输出11
        System.out.println("JamesBeanDefinitionProcessor..postProcessBeanFactory(),Bean的数量" + beanFactory.getBeanDefinitionCount());
    }

    /**
     * 这个方法【最先执行】
     *
     * @param registry
     * @throws BeansException
     */
    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {

        //bean的数量是10
        System.out.println("JamesBeanDefinition.postProcessBeanDefinitionRegistry()...bean的数量" + registry.getBeanDefinitionCount());

        //创建注册bean方式 1
        //RootBeanDefinition rbd = new RootBeanDefinition(Moon.class);

        //创建注册bean方式 2
        AbstractBeanDefinition rbd = BeanDefinitionBuilder.rootBeanDefinition(Moon.class).getBeanDefinition();

        //新注册了一个bean
        registry.registerBeanDefinition("hello", rbd);
    }

}
