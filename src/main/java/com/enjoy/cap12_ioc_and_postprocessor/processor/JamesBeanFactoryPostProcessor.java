package com.enjoy.cap12_ioc_and_postprocessor.processor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * 注意：BeanFactoryPostProcessor不同于cap7的BeanPostProcessor(CustomBeanPostProcessor类)
 * 可以用beanFactory.点以下看有什么方法
 */
@Component
public class JamesBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        System.out.println("JamesBeanFactoryPostProcessor.....调到了BeanFactoryPostProcessor.postProcessBeanFactory()");
        //所有bean的定义，已经加载到beanFactory, 但是bean实例还没创建
        int count = beanFactory.getBeanDefinitionCount();
        //当前bean的名称数组，可debug到line24,查看beanDefName数据
        String[] beanDefName = beanFactory.getBeanDefinitionNames();
        System.out.println("当前BeanFactory中有" + count + "个Bean");
        System.out.println(Arrays.asList(beanDefName));

    }

}
