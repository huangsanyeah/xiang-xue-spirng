package com.enjoy.cap12_ioc_and_postprocessor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.enjoy.cap9_qualifier_primary_resource_inject_annotation.bean.Moon;

@Configuration
@ComponentScan("com.enjoy.cap12_ioc_and_postprocessor.processor")
public class Cap12MainConfig {

	/**
	 * bean的id，是getMoon，默认方法名
	 * 见Captest12Test输出的最后一行：
	 * 当前BeanFactory中有9个Bean
	 * [org.springframework.context.annotation.internalConfigurationAnnotationProcessor,
	 * org.springframework.context.annotation.internalAutowiredAnnotationProcessor,
	 * org.springframework.context.annotation.internalRequiredAnnotationProcessor,
	 * org.springframework.context.annotation.internalCommonAnnotationProcessor,
	 * org.springframework.context.event.internalEventListenerProcessor,
	 * org.springframework.context.event.internalEventListenerFactory,
	 * cap12MainConfig,
	 * jamesBeanFactoryPostProcessor,
	 * getMoon]
	 * @return
	 */
	@Bean
	public Moon getMoon(){
		return new Moon();
	}
}
