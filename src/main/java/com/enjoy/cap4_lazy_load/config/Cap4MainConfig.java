package com.enjoy.cap4_lazy_load.config;

import com.enjoy.cap1_xml_to_configuration.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
public class Cap4MainConfig {

    /**
     * 懒加载：仅当使用或者获取的时候才会创建bean,bean的创建在容器之后
     */
    @Lazy
    @Bean
    public Person person() {
        System.out.println("给容器中添加一个person");
        return new Person("james", 20);
    }
}
