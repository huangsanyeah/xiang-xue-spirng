package com.enjoy.cap2_component_scan.config;

import com.enjoy.cap1_xml_to_configuration.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Controller;

/**
 * TODO 注意：
 * 配置includeFilters时，需要设置useDefaultFilters=false
 * 配置excludeFilters时，需要设置useDefaultFilters=true
 */
@Configuration
//@Controller  @Service  @Respostry  @Component

//1.1 Filter的includeFilters根据【注解ANNOTATION】确定ComponentScan的扫描范围，需要设置useDefaultFilters=false
/*@ComponentScan(value="com.enjoy.cap2", includeFilters={
		@ComponentScan.Filter(type= FilterType.ANNOTATION, classes={Controller.class})
}, useDefaultFilters=false)*/


//1.2 Filter的includeFilters根据【类型ASSIGNABLE_TYPE】确定ComponentScan的扫描范围（OrderController），需要设置useDefaultFilters=false
/*@ComponentScan(value="com.enjoy.cap2", includeFilters={
		@ComponentScan.Filter(type= FilterType.ASSIGNABLE_TYPE, classes={OrderController.class})
}, useDefaultFilters=false)*/


//1.3 Filter的includeFilters根据【自定义CUSTOM】的配置类CustomerTypeFilter确定ComponentScan的扫描范围，需要设置useDefaultFilters=false
/*@ComponentScan(value= "com.enjoy.cap2_component_scan", includeFilters={
		@ComponentScan.Filter(type= FilterType.CUSTOM, classes={CustomerTypeFilter.class})
}, useDefaultFilters=false)*/


/**
 * TODO 这里useDefaultFilters=true为什么是true ？
 * 因为设置了useDefaultFilters=true，spring会默认扫描所有标识了@Component注解的bean,如@controller,@service等都属于@Component标识的
 * 此处是excludeFilters，即先将所有bean扫描然后排除指定的bean,如果设置useDefaultFilters=false那么就无法扫描所有被@Component标识的bean了
 */
//2.Filter的excludeFilters确定ComponentScan需要排除的扫描范围，默认配置设置为useDefaultFilters=true
@ComponentScan(value = "com.enjoy.cap2_component_scan", excludeFilters = {
        @ComponentScan.Filter(type = FilterType.ANNOTATION, classes = {Controller.class})
}, useDefaultFilters = true)


//3.扫描所有
//@ComponentScan(value="com.enjoy.cap2_component_scan")
public class Cap2MainConfig {
    //给容器中注册一个bean, 类型为返回值的类型,
    @Bean
    public Person person01() {
        return new Person("james", 20);
    }
}
