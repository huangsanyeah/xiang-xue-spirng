package com.enjoy.cap8_value_annotation.config;

import com.enjoy.cap8_value_annotation.bean.Bird;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
//将test.properties配置文件加载起来
@PropertySource(value="classpath:/test.properties")
public class Cap8MainConfig {
	@Bean
	public Bird bird(){
		return new Bird();
	}
}
