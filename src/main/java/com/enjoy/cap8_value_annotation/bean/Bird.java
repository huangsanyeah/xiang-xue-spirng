package com.enjoy.cap8_value_annotation.bean;

import org.springframework.beans.factory.annotation.Value;

/**
 * 使用@Value进行赋值:1,基本字符  2,springEL表达式, 3,可以读取 我们的配置文件
 */
public class Bird {
    /**
     * 1.使用@Value赋默认值
     */
    @Value("James")
    private String name;

    /**
     * 2.el表达式，值为18
     */
    @Value("#{20-2}")
    private Integer age;

    /**
     * 3.读取配置文件【常用】,注意Configuration类中使用了@PropertySource(value="classpath:/test.properties")引入配置文件
	 * springboot yml可直接读取
     */
    @Value("${bird.color}")
    private String color;

    public Bird() {
        super();
    }

    public Bird(String name, Integer age, String color) {
        super();
        this.name = name;
        this.age = age;
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Bird [name=" + name + ", age=" + age + ", color=" + color + "]";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

}
