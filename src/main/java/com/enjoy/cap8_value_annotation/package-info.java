/**
 * @Value注解的使用：
 * 1.基本字符
 * 2.springEL表达式,
 * 3.可以读取 我们的配置文件test.properties【@PropertySource(value="classpath:/test.properties")】
 * 4.从环境变量中拿配置信息 ConfigurableEnvironment
 */
package com.enjoy.cap8_value_annotation;
