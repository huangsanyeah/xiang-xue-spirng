import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.enjoy.cap12_ioc_and_postprocessor.Cap12MainConfig;

/**
 * 课时9：
 * 1.BeanFactoryPostProcessor: beanFactory的后置处理器
 * 2.BeanDefinitionPostProcessor:
 * 3.IOC源码解析
 * 4.BeanFactory拿bean，拿得到返回，拿不到创建；FactoryBean是工厂模式创建bean
 */
public class Cap12Test {
    @Test
    public void test01() {
        //点击AnnotationConfigApplicationContext类（不是构造），一层一层往上跟
        // public interface ApplicationContext extends EnvironmentCapable, ListableBeanFactory, HierarchicalBeanFactory,MessageSource, ApplicationEventPublisher, ResourcePatternResolver
        //发现继承了ListableBeanFactory。即AnnotationConfigApplicationContext也实现了beanFactory的部分功能
        AnnotationConfigApplicationContext app = new AnnotationConfigApplicationContext(Cap12MainConfig.class);

        //销毁容器
        app.close();


    }
}
