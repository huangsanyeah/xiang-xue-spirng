import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.enjoy.cap5_conditional.config.Cap5MainConfig;

/**
 * 输出：
 * 操作系统的名字:Windows 10
 * 一月 29, 2020 3:13:11 下午 org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor <init>
 * 信息: JSR-330 'javax.inject.Inject' annotation found and supported for autowiring
 * 给容器中添加person.......
 * 给容器中添加lison.......
 * IOC容器创建完成........
 * <p>
 * 想要看linux的输出，自行配置editConfigurations中VM options中设置参数为-Dos.name=linux就可以模拟当前环境为linux
 */
public class Cap5Test {
    @Test
    public void test01() {
        AnnotationConfigApplicationContext app = new AnnotationConfigApplicationContext(Cap5MainConfig.class);

        System.out.println("IOC容器创建完成........");


    }
}
