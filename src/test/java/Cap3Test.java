import com.enjoy.cap3_singleton_prototype.config.Cap3MainConfig;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * 测试bean是单例 还是原型 单例输出true 原型false，更改Cap3MainConfig中的@Scope查看效果，注意默认是单例
 */
public class Cap3Test {
    @Test
    public void test01() {
        AnnotationConfigApplicationContext app = new AnnotationConfigApplicationContext(Cap3MainConfig.class);
        String[] names = app.getBeanDefinitionNames();
        for (String name : names) {
            System.out.println(name);
        }
        //从容器中分别取两次person实例, 看是否为同一个bean
        Object bean1 = app.getBean("person");
        Object bean2 = app.getBean("person");
        System.out.println(bean1 == bean2);
        //结论:bean1==bean2,是同一个对象，那么就是单例的
    }
}
