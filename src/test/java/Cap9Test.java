import com.enjoy.cap9_qualifier_primary_resource_inject_annotation.bean.Moon;
import com.enjoy.cap9_qualifier_primary_resource_inject_annotation.bean.Sun;
import com.enjoy.cap9_qualifier_primary_resource_inject_annotation.config.Cap9MainConfig;
import com.enjoy.cap9_qualifier_primary_resource_inject_annotation.dao.TestDao;
import com.enjoy.cap9_qualifier_primary_resource_inject_annotation.service.TestService;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * 1.@Autowired注解
 * 2.有参构造注入bean
 * 3.Aware：com.enjoy.cap9_qualifier_primary_resource_inject_annotation.bean.Light.java
 * 测试的时候分别注释掉17-21，25-30查看输出
 *
 */
public class Cap9Test {

    /**
     * service取出的testDao的bean与从容器中取出的进行比较
     */
    @Test
    public void test01() {
        AnnotationConfigApplicationContext app = new AnnotationConfigApplicationContext(Cap9MainConfig.class);
        System.out.println(app);

        //1.取出service中的dao并打印
	    TestService testService = app.getBean(TestService.class);
		testService.println();
        //2直接从容器中获取TestDao, 和使用Autowired注解来取比较
		TestDao testDao = app.getBean(TestDao.class);
		System.out.println(testDao);



        app.close();


    }

    /**
     * 使用构造器的方式注入bean，即在Sun中注入Moon,这个Moon跟从容器中拿出的Moon是同一个
     */
    @Test
    public void test02() {
        AnnotationConfigApplicationContext app = new AnnotationConfigApplicationContext(Cap9MainConfig.class);
		Moon moon = (Moon)app.getBean(Moon.class);
		System.out.println(moon);

		Sun sun = (Sun)app.getBean(Sun.class);
		System.out.println(sun.getMoon());


        app.close();


    }
}
