import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.enjoy.cap4_lazy_load.config.Cap4MainConfig;

/**
 * 测试懒加载，对比注释掉@Lazy的先后区别
 * 输出结果：
 * 1.有Lazy注解的时候
 * IOC容器创建完成
 * 给容器中添加一个person
 * 2.没有Lazy注解的时候
 * 给容器中添加一个person
 * IOC容器创建完成
 */
public class Cap4Test {
    @Test
    public void test01() {
        //创建IOC容器
        AnnotationConfigApplicationContext app = new AnnotationConfigApplicationContext(Cap4MainConfig.class);
        //容器中的bean未创建，此时并不会打印“给容器中添加一个person”
        System.out.println("IOC容器创建完成");
		//执行获取的时候才创建并初始化bean
        app.getBean("person");
    }
}
