import com.enjoy.cap6_import.config.Cap6MainConfig;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @import注解测试类
 */
public class Cap6Test {
	@Test
	public void test01(){
		AnnotationConfigApplicationContext app = new AnnotationConfigApplicationContext(Cap6MainConfig.class);
		
		System.out.println("IOC容器创建完成........");

		//不加特殊符号&,获取Money的Bean，jamesFactoryBean是Cap6MainConfig的jamesFactoryBean()方法名
		Object bean1 = app.getBean("jamesFactoryBean");
		Object bean2 = app.getBean("jamesFactoryBean");
		System.out.println("bean的类型="+bean1.getClass());
		System.out.println(bean1 == bean2);

		/*
		 * 1.这里面加了特殊符号& 则取出的是CustomFactoryBean，而不是Monkey
		 * 2.见BeanFactory接口中isFactoryDereference方法
		 * String FACTORY_BEAN_PREFIX = "&";
		 * public static boolean isFactoryDereference(@Nullable String name) {
		 * 		return (name != null && name.startsWith(BeanFactory.FACTORY_BEAN_PREFIX));
		 *        }
		 */
		Object customFactoryBean = app.getBean("&jamesFactoryBean");
		
		String[] beanDefinitionNames = app.getBeanDefinitionNames();
		for(String name:beanDefinitionNames){
			System.out.println(name);
		}

	}
}
