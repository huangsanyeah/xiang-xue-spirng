import com.enjoy.cap8_value_annotation.bean.Bird;
import com.enjoy.cap8_value_annotation.config.Cap8MainConfig;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

/***
 * @description 测试@Value注解,使用@Value进行赋值:
 * 1.基本字符
 * 2.springEL表达式,
 * 3.可以读取 我们的配置文件test.properties【@PropertySource(value="classpath:/test.properties")】
 * 4.从环境变量中拿配置信息 ConfigurableEnvironment
 * @date Created in 2020/1/31-12:04
 */
public class Cap8Test {
    @Test
    public void test01() {
        AnnotationConfigApplicationContext app = new AnnotationConfigApplicationContext(Cap8MainConfig.class);
        //从容器中获取所有bean
        String[] names = app.getBeanDefinitionNames();

        for (String name : names) {
            System.out.println(name);
        }
        Bird bird = (Bird) app.getBean("bird");

        System.out.println(bird);
        System.out.println("IOC容器创建完成........");

		//从环境变量中拿配置信息
        ConfigurableEnvironment environment = app.getEnvironment();
        System.out.println("environment====" + environment.getProperty("bird.color"));
        app.close();


    }
}
