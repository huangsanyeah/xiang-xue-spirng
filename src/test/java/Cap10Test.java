import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.enjoy.cap10_aop_basic_concepts.aop.Calculator;
import com.enjoy.cap10_aop_basic_concepts.config.Cap10MainConfig;

/**
 * 课时6，7：AOP测试类
 */
public class Cap10Test {
    @Test
    public void test01() {
        AnnotationConfigApplicationContext app = new AnnotationConfigApplicationContext(Cap10MainConfig.class);
        //这里面要从spring中拿bean，直接new对象就没有使用spring，就是一个对象，不是bean,则无法执行AOP切入方法了
        //Calculator c = new Calculator();
        Calculator c = app.getBean(Calculator.class);
        //TODO 下面这个方法，打断点，然后debug启动进去该方法，发现是通过代理执行的方法
        int result = c.div(4, 2);
        System.out.println("div方法的结果是：" + result);
        app.close();


    }
}
